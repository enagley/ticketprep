# README #

## ticketPrep.py ##

Utilizes EM7 API to Prep Support Tickets

The Script can:

* Pre-populate and submit the First Contact Note
* Add customer as watcher to ticket
* Send First Response Note

### How do I get set up? ###

* Download or clone this repository
* Edit the following Variables for your information
    + un - Line 31
    + pw - Line 32
    + baseURL - Line 33



### How do I use it? ###

* cd to the directory where you downloaded the files.
* `python ticketPrep.py --help` will show you the options

### Who do I talk to? ###

* EricN

# Usage #

```
python ticketPrep.py [-h] [-u USER] [-n] t

Populates and Submits Initial Ticket Template, Adds user as Watcher and sends Initial Contact Note.

positional arguments:
  t                     Ticket ID

optional arguments:
  -h, --help            show this help message and exit
  -u USER, --user USER  Contact's User ID
  -n, --new             Option for new tickets
```