import argparse
import requests


def get_args():
    parser = argparse.ArgumentParser(description='Populates and Submits \
                                     Initial Ticket Template, Adds user as \
                                     Watcher and sends Initial Contact Note.')
    parser.add_argument('tid', metavar='t', help="Ticket ID")
    parser.add_argument('-u', '--user', help="Contact's User ID")
    parser.add_argument('-n', '--new', help="Option for new tickets",
                        action="store_true")
    args = parser.parse_args()
    return args.tid, args.user, args.new


def apiGet(url, payload=None):
    if payload is None:
        dictionary = requests.get(url).json()
    else:
        dictionary = requests.get(url, params=payload).json()
    return dictionary


def apiPost(url, data, job):
    response = requests.post(url, json=data)
    print(job + ' HTTP Response: ' + str(response.status_code))


un = 'em7admin'
pw = 'em7adminS'
# baseURL = 'http://' + un + ':' + pw + '@192.168.33.84/'
baseURL = 'http://' + un + ':' + pw + '@em7.sciencelogic.com'

tzList = []
with open('EM7_timezones', 'r') as inf:
    for line in inf:
        tzList.append(eval(line))

tid, uid, optNew = get_args()
ticketURL = baseURL + "/api/ticket/" + tid
ticketInfo = apiGet(ticketURL)
tDesc = ticketInfo["description"]

orgURL = baseURL + ticketInfo['organization']
orgInfo = apiGet(orgURL)
orgCustFields = (orgInfo['custom_fields'])
em7Version = orgCustFields['32_3']

if uid:
    custURL = baseURL + '/api/account/' + uid
elif optNew:
    userPayload = {'hide_filterinfo': 1,
                   'filter.0.user.eq': ticketInfo['aligned_resource_name']}
    userURL = baseURL + '/api/account'
    rawUser = apiGet(userURL, userPayload)
    filteredUser = rawUser[0]
    custURL = baseURL + filteredUser['URI']
else:
    custURL = baseURL + ticketInfo["opened_by"]

custInfo = apiGet(custURL)

custName = custInfo['contact_fname'] + ' ' + custInfo['contact_lname']
custPhone = custInfo['phone']
custEmail = custInfo['email']
custTZ = tzList[int(custInfo['timezone'])]

if custPhone == '':
    custPhone = 'Not Set'
if custEmail == '':
    custEmail = 'Not Set'

tnote = '<span style=\"font-size: x-large;\"><strong>Initial Ticket/Contact \
         Template (cloaked)</strong></span><br> <br> <table style=\"width: \
         450px;\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\"> <tbody> \
         <tr> <td width=\"240px\"><strong>Contact Name</strong></td> <td \
         width=\"550px\">&nbsp;' + custName + '</td> </tr> <tr> <td><strong> \
         Contact Phone</strong></td> <td>&nbsp;' + custPhone + '<br> </td> \
         </tr> <tr> <td><strong>Contact Email</strong></td> <td>&nbsp;'
tnote += custEmail + '<br> </td> </tr> <tr> <td><strong>Customer Time Zone \
         </strong></td> <td>&nbsp;' + custTZ + '<br> </td> </tr> </tbody> \
         </table> <br><strong>Issue Description:</strong><br><p>'
tnote += tDesc + '<br> </p>'

noteData = {}
noteData['note_text'] = tnote
noteData['hidden'] = 1

noteURL = ticketURL + "/note"

apiPost(noteURL, noteData, 'First Contact Note')

if uid or optNew:
    watcherURL = ticketURL + "/watcher_org"
    watcherData = {}
    if uid:
        watcherData['watcher'] = '/api/account/' + uid
    elif optNew:
        watcherData['watcher'] = filteredUser['URI']
    watcherData['email_1'] = 1

    apiPost(watcherURL, watcherData, 'Set Watcher')

if uid or optNew:
    tnote2 = '<p><br> Thank you for contacting ScienceLogic Technical Support \
              . &nbsp; We have completed our initial review and <strong>one \
              of our Technical Support Engineers has been assigned</strong>. \
              &nbsp; You will receive updates and communications as our \
              investigation progresses.&nbsp;</p> <p>&nbsp;If you are \
              reporting a time sensitive or operational impacting item, \
              please also call our Support line and reference your ticket \
              number. &nbsp;Please follow this link for our current Support \
              numbers \
              <a href=\"https://portal.sciencelogic.com/portal/contact-us\"> \
              https://portal.sciencelogic.com/portal/contact-us</a></p> \
              <p>Should you have any additional information that would be \
              helpful in our investigation, please include via note or \
              attachment to the ticket (i.e. screenshots, logs, etc.).&nbsp; \
              We look forward to serving you,<br> <br> ~ ScienceLogic \
              Technical Support</p> <p><br> </p>'
    note2Data = {}
    note2Data['note_text'] = tnote2
    note2Data['hidden'] = 0
    apiPost(noteURL, note2Data, 'First Response Note')

    ticketData = {}
    ticketData['custom_fields'] = {'2_1': em7Version}
    apiPost(ticketURL, ticketData, 'Set EM7 Version')

print('Done')
